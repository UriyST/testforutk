package com.testproject;

import com.testproject.entity.User;
import com.testproject.entity.WebServer;

public interface ISession {
	void login(WebServer webServer, User user);

	void logout();
}
