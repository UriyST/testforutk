package com.testproject;

import com.testproject.connection.IWebServerConnection;
import com.testproject.entity.User;
import com.testproject.entity.WebServer;
import com.testproject.factory.Factory;

public class WebSession implements ISession {
	private IWebServerConnection webServerConection;
	@SuppressWarnings("unused")
	private User user;
	@SuppressWarnings("unused")
	private String token;

	public void login(WebServer webServer, User user) {
		this.user = user;
		connectToServer(webServer);
	}

	public void logout() {
		webServerConection.disconnectFromServer();
	}

	private void connectToServer(WebServer webServer) {
		webServerConection = Factory.getWebServerConnection();
		webServerConection.connectToServer(webServer);
	}
}
