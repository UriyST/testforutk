package com.testproject.factory;

import com.testproject.ISession;
import com.testproject.WebSession;
import com.testproject.connection.IWebServerConnection;
import com.testproject.connection.WebServerConection;
import com.testproject.service.IVideoService;
import com.testproject.service.WebVideoService;

public class Factory {
	public static IWebServerConnection getWebServerConnection() {
		return new WebServerConection();
	}

	public static ISession getSession() {
		return new WebSession();
	}

	public static IVideoService getVideoService() {
		return new WebVideoService();
	}
	
}
