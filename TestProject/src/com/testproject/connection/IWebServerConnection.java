package com.testproject.connection;

import com.testproject.entity.WebServer;

public interface IWebServerConnection {
	void connectToServer(WebServer webServer);

	void disconnectFromServer();

	void sendGet();

	void sendPost();
}
