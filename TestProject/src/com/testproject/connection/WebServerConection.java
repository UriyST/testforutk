package com.testproject.connection;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.util.Scanner;

import org.json.JSONException;
import org.json.JSONObject;

import android.os.Build;

import com.testproject.entity.WebServer;

public class WebServerConection implements IWebServerConnection {
	@SuppressWarnings("unused")
	private WebServer webServer;

	public void connectToServer(WebServer webServer) {
		this.webServer = webServer;
	}

	public void disconnectFromServer() {

	}

	private static void disableConnectionReuseIfNecessary() {
		if (Integer.parseInt(Build.VERSION.SDK) < Build.VERSION_CODES.FROYO) {
			System.setProperty("http.keepAlive", "false");
		}
	}

	private static String getResponseText(InputStream inStream) {
		return new Scanner(inStream).useDelimiter("\\A").next();
	}

	public String queryRESTurl(String url) {
		disableConnectionReuseIfNecessary();

		HttpURLConnection urlConnection = null;
		try {
			URL urlToRequest = new URL("http://vacancy.dev.telehouse-ua.net/");
			urlConnection = (HttpURLConnection) urlToRequest.openConnection();
			urlConnection.setConnectTimeout(1000);
			urlConnection.setReadTimeout(1000);

			int statusCode = urlConnection.getResponseCode();
			if (statusCode == HttpURLConnection.HTTP_UNAUTHORIZED) {
			} else if (statusCode != HttpURLConnection.HTTP_OK) {
			}

			InputStream in = new BufferedInputStream(
					urlConnection.getInputStream());
			return new JSONObject(getResponseText(in)).toString();

		} catch (MalformedURLException e) {
		} catch (SocketTimeoutException e) {
		} catch (IOException e) {
		} catch (JSONException e) {
		} finally {
			if (urlConnection != null) {
				urlConnection.disconnect();
			}
		}

		return null;
	}

	public void sendPost() {
	}

	@Override
	public void sendGet() {
		// TODO Auto-generated method stub

	}

}
