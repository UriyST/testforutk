package com.testproject.service;

import java.util.List;

public interface IVideoService {
	List<?> getVideoList();

	Class<?> getVideoById(String videoId);
}
